package com.soapserver.core.validator;

import com.soapserver.entities.HotelsRequest;

import java.util.regex.Pattern;

public class HotelRequestValidator {
    private final static String STRING_PATTERN = "[a-zA-Zа-яА-Я]+";
    private final static String CATEGORY_PATTERN = "[0-5]";

    public static boolean isRequestOk(HotelsRequest request){
        return (isFieldOk(request.getLocation().getCode()) &&
                isFieldOk(request.getLocation().getCityName())
                && isLanguage(request.getLanguage()) &&
                isFieldOk(request.getNamePart()) &&
                isCategory(request.getCategory()));
    }

    private static boolean isFieldOk(String field){
        return (field.isEmpty() || Pattern.matches(STRING_PATTERN, field));
    }

    private static boolean isLanguage(String lang){
        return ("ru".equals(lang) || "en".equals(lang));
    }

    private static boolean isCategory(String category){
        return (category.isEmpty() || Pattern.matches(CATEGORY_PATTERN, category));
    }
}
