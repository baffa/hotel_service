package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.core.validator.HotelRequestValidator;
import com.soapserver.entities.HotelsRequest;

public class CheckRequestFilter implements PreFilter<HotelsRequest> {

    @Override
    public boolean isApplicable(HotelsRequest request) {
        return true;
    }

    @Override
    public void preProcess(HotelsRequest request) throws ServiceException {
        if (!HotelRequestValidator.isRequestOk(request)){
            throw new ServiceException("Request is not valid");
        }
    }
}
