package com.soapserver.core.filters.impl;

import com.soapserver.core.filters.PostFilter;
import com.soapserver.entities.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GroupHotelsByCityFilter implements PostFilter<HotelsRequest, HotelsResponse> {
    @Override
    public boolean isApplicable(final HotelsRequest request, final HotelsResponse response) {
        return !response.getHotelInfo().isEmpty();
    }

    @Override
    public void postProcess(final HotelsRequest request, final HotelsResponse response) {
        List<HotelInfo> hotelInfoDeleteList = new ArrayList<>();
        Map<String, List<Hotel>> hotelsMap = new HashMap<>();
        for (HotelInfo hotelInfo : response.getHotelInfo()) {
            String cityName = hotelInfo.getCity().getName();
            if (hotelsMap.containsKey(cityName)) {
                hotelInfoDeleteList.add(hotelInfo);
            }

            List<Hotel> hotels = hotelsMap.get(cityName);
            if (hotels == null) {
                hotels = new ArrayList<>();
                hotelsMap.put(cityName, hotels);
            }
            hotels.addAll(hotelInfo.getHotels().getHotel());

        }

        response.getHotelInfo().removeAll(hotelInfoDeleteList);


        for (HotelInfo hotelInfo : response.getHotelInfo()) {
            Hotels hotels = hotelInfo.getHotels();
            if (hotels == null) {
                continue;
            }

            hotels.getHotel().clear();

            List<Hotel> hotelList = hotelsMap.get(hotelInfo.getCity().getName());
            if (hotelList != null) {
                hotels.getHotel().addAll(hotelList);
            }
        }
    }
}


