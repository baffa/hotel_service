package com.soapserver.core.filters.impl;

import java.util.List;

import com.soapserver.core.dao.CountriesDAO;
import com.soapserver.core.filters.PreFilter;
import com.soapserver.core.processors.ServiceException;
import com.soapserver.entities.HotelsRequest;

public class CheckCountryCodeFilter implements PreFilter<HotelsRequest> {
	
	private CountriesDAO countriesDAO;
	
	@Override
	public boolean isApplicable(final HotelsRequest request) {
		return true;
	}

	@Override
	public void preProcess(final HotelsRequest request) throws ServiceException {
		if (request.getLocation().getCode() != null) {
			final List<String> countryCodes = countriesDAO.getCountriesCodes();
			if (!countryCodes.contains(request.getLocation().getCode())) {
				throw new ServiceException("Requested country code is incorrect");
			}
		}
	}
	
	public void setCountriesDAO(CountriesDAO countriesDAO) {
		this.countriesDAO = countriesDAO;
	}
	
}
