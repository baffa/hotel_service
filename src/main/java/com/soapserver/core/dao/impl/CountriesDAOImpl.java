package com.soapserver.core.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.soapserver.core.dao.CountriesDAO;
import com.soapserver.core.dao.JdbcExt;
import org.springframework.jdbc.core.RowMapper;

public class CountriesDAOImpl extends JdbcExt implements CountriesDAO {

	private static final String COUNTRIES_CODES = "SELECT CODE FROM GPT_LOCATION WHERE TYPE_ID = 3";

	@Override
	public List<String> getCountriesCodes(){
		return getJdbcTemplate().query(COUNTRIES_CODES, new RowMapper<String>() {
			@Override
			public String mapRow(ResultSet resultSet, int i) throws SQLException {
				return resultSet.getString("CODE");
			}
		});
	}
}
