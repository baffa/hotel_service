<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:sutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.StringUtil"
                xmlns:queryutil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.SqlQueriesUtil"
                exclude-result-prefixes="ent sutil queryutil">

    <xsl:output method="xml"/>

    <xsl:template match="/">

        <ent:HotelsResponse>
            <xsl:for-each select="Result/Entry">
                <xsl:variable name="hotelId" select="ID"/>
                <xsl:variable name="countryName" select="COUNTRY_NAME"/>
                <xsl:variable name="countryCode" select="COUNTRY_CODE"/>
                <xsl:variable name="cityName" select="CITY_NAME"/>
                <xsl:variable name="cityCode" select="CITY_CODE"/>
                <xsl:variable name="hotelName" select="NAME"/>
                <xsl:variable name="hotelCode" select="CODE"/>
                <xsl:variable name="phone" select="PHONE"/>
                <xsl:variable name="fax" select="FAX"/>
                <xsl:variable name="email" select="EMAIL"/>
                <xsl:variable name="url" select="URL"/>
                <xsl:variable name="category" select="CATEGORY_ID"/>
                <xsl:variable name="checkIn" select="CHECK_IN"/>
                <xsl:variable name="checkOut" select="CHECK_OUT"/>
                <xsl:variable name="longtitude" select="LONGTITUDE"/>
                <xsl:variable name="latitude" select="LATITUDE"/>
                <xsl:variable name="images"
                              select="queryutil:subQuery(concat('SELECT GPT_HOTEL_IMAGES.URL, TYPE_NAME FROM GPT_HOTEL JOIN GPT_HOTEL_IMAGES JOIN GPT_HOTEL_IMAGE_TYPE ON GPT_HOTEL.ID = HOTEL_ID WHERE TYPE_ID = GPT_HOTEL_IMAGE_TYPE.ID AND GPT_HOTEL.ID = ', $hotelId))"/>

                <ent:hotel_info>
                    <ent:country>
                        <ent:name>
                            <xsl:value-of select="$countryName"/>
                        </ent:name>
                        <ent:code>
                            <xsl:value-of select="$countryCode"/>
                        </ent:code>
                    </ent:country>
                    <ent:city>
                        <ent:name>
                            <xsl:value-of select="$cityName"/>
                        </ent:name>
                        <ent:code>
                            <xsl:value-of select="$cityCode"/>
                        </ent:code>
                    </ent:city>
                    <ent:hotels>
                        <ent:hotel>
                            <ent:name>
                                <xsl:value-of select="$hotelName"/>
                            </ent:name>
                            <ent:code>
                                <xsl:value-of select="$hotelCode"/>
                            </ent:code>
                            <ent:phone>
                                <xsl:value-of select="$phone"/>
                            </ent:phone>
                            <ent:fax>
                                <xsl:value-of select="$fax"/>
                            </ent:fax>
                            <ent:email>
                                <xsl:value-of select="$email"/>
                            </ent:email>
                            <ent:url>
                                <xsl:value-of select="$url"/>
                            </ent:url>
                            <ent:category>
                                <xsl:value-of select="$category"/>
                            </ent:category>
                            <ent:check_in>
                                <xsl:value-of select="$checkIn"/>
                            </ent:check_in>
                            <ent:check_out>
                                <xsl:value-of select="$checkOut"/>
                            </ent:check_out>
                            <ent:coordinates>
                                <ent:longtitude>
                                    <xsl:value-of select="$longtitude"/>
                                </ent:longtitude>
                                <ent:latitude>
                                    <xsl:value-of select="$latitude"/>
                                </ent:latitude>
                            </ent:coordinates>
                            <ent:images>
                                <xsl:for-each select="$images/Entry">
                                    <ent:image>
                                        <ent:url>
                                            <xsl:value-of select="./URL"/>
                                        </ent:url>
                                        <ent:type>
                                            <xsl:value-of select="./TYPE_NAME"/>
                                        </ent:type>
                                    </ent:image>
                                </xsl:for-each>
                            </ent:images>
                        </ent:hotel>
                    </ent:hotels>
                </ent:hotel_info>
            </xsl:for-each>
        </ent:HotelsResponse>

        <!--<ent:CountryResponse>-->
        <!--<xsl:for-each select="Result/Entry">-->
        <!--<xsl:variable name="cityName" select="city_name"/>-->
        <!--<xsl:variable name="currencyId" select="currency_id"/>-->
        <!--<xsl:variable name="currency" select="sutil:upperCase(string($currenciesRS/Entry[id = $currencyId]/code))"/>-->

        <!--<ent:Country>-->
        <!--<ent:Name>-->
        <!--<xsl:value-of select="country_name"/>-->
        <!--</ent:Name>-->
        <!--<ent:Cities>-->
        <!--<ent:City Name="{$cityName}" />-->
        <!--</ent:Cities>-->
        <!--<ent:Currency>-->
        <!--<xsl:value-of select="$currency"/>-->
        <!--</ent:Currency>-->
        <!--</ent:Country>-->
        <!--</xsl:for-each>-->
        <!--</ent:CountryResponse>-->
    </xsl:template>

</xsl:stylesheet>