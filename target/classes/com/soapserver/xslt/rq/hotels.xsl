<?xml version="1.0"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:ent="http://soapserver.com/entities"
                xmlns:requtil="http://xml.apache.org/xalan/java/com.soapserver.core.helpers.RequestUtil"
                exclude-result-prefixes="ent requtil">

    <xsl:output method="text"/>

    <xsl:template match="/">
        <xsl:variable name="code" select="ent:HotelsRequest/ent:location/ent:code"/>
        <xsl:variable name="cityName" select="ent:HotelsRequest/ent:location/ent:cityName"/>
        <xsl:variable name="namePart" select="ent:HotelsRequest/ent:namePart"/>
        <xsl:variable name="category" select="ent:HotelsRequest/ent:category"/>
        <xsl:variable name="language">
            <xsl:choose>
                <xsl:when test="string(ent:HotelsRequest/ent:language) = 'en'">
                    <xsl:value-of select="1"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="2"/>
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>

        <xsl:choose>
            <xsl:when test="$code = '' or $language = ''">
                <xsl:text>Wrong request</xsl:text>
                <xsl:message>
                    <xsl:value-of select="requtil:overrideResponse()"/>
                </xsl:message>
            </xsl:when>
            <xsl:otherwise>
                <xsl:text>SELECT GPT_HOTEL.ID, CITY_COUNTRY.COUNTRY_NAME,  CITY_COUNTRY.COUNTRY_CODE, CITY_COUNTRY.CITY_NAME,
                    CITY_COUNTRY.CITY_CODE,  GPT_HOTEL_NAME.NAME, GPT_HOTEL.CODE, PHONE, FAX, EMAIL, URL, CATEGORY_ID,
                    CHECK_IN, CHECK_OUT, LONGTITUDE, LATITUDE
                    FROM GPT_HOTEL JOIN (SELECT GPT_LOCATION.ID,
                    GPT_LOCATION_NAME.NAME AS CITY_NAME, GPT_LOCATION.CODE AS CITY_CODE,
                    COUNTRY.NAME AS COUNTRY_NAME, COUNTRY.CODE AS COUNTRY_CODE
                    FROM GPT_LOCATION JOIN (SELECT ID, NAME, CODE FROM GPT_LOCATION JOIN GPT_LOCATION_NAME
                    ON GPT_LOCATION.ID = LOCATION_ID WHERE LANG_ID =</xsl:text>
                <xsl:value-of select="$language"/>
                <xsl:text> AND CODE = '</xsl:text>
                <xsl:value-of select="$code"/>
                <xsl:text>' AND TYPE_ID = 3)
                    AS COUNTRY JOIN GPT_LOCATION_NAME ON GPT_LOCATION.ID = LOCATION_ID
                    WHERE GPT_LOCATION.PARENT = COUNTRY.ID AND </xsl:text>
                <xsl:if test="not($cityName = '')"><xsl:text>GPT_LOCATION_NAME.NAME = '</xsl:text>
                    <xsl:value-of select="$cityName"/>
                    <xsl:text>' AND </xsl:text>
                </xsl:if>
                <xsl:text> GPT_LOCATION.TYPE_ID = 4 AND LANG_ID =</xsl:text>
                <xsl:value-of select="$language"/>
                <xsl:text>) AS CITY_COUNTRY JOIN GPT_HOTEL_NAME ON GPT_HOTEL.ID = HOTEL_ID WHERE CITY_ID = CITY_COUNTRY.ID
                AND LANG_ID = '</xsl:text><xsl:value-of select="$language"/><xsl:text>' </xsl:text>
                <xsl:if test="not($namePart = '')"><xsl:text>AND GPT_HOTEL_NAME.NAME LIKE '%</xsl:text>
                    <xsl:value-of select="$namePart"/><xsl:text>%' </xsl:text>
                </xsl:if>
                <xsl:if test="not($category = '')"><xsl:text>AND CATEGORY_ID = '</xsl:text>
                    <xsl:value-of select="$category"/><xsl:text>'</xsl:text>
                </xsl:if>
                <!--<xsl:text>select co.name as country_name, co.currency_id, ci.name as city_name, ci.active from countries co join cities ci on co.country_id=ci.country_id</xsl:text>-->
                <!--<xsl:if test="string-length($countryName) &gt; 0">-->
                    <!--<xsl:text> where co.name="</xsl:text>-->
                    <!--<xsl:value-of select="$countryName"/>-->
                    <!--<xsl:text>"</xsl:text>-->
                <!--</xsl:if>-->
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>